use specs::prelude::*;
use super::components::{
    Position,
    Renderable,
};

/// create tile entity
pub fn create_map(world: &mut World, map_path: String) -> Result<(), String> {
    let mut loader = tiled::Loader::new();
    let map = loader
        .load_tmx_map(map_path)
        .map_err(|e| e.to_string())?;

    // tilesheet 
    let tilesheet = map.tilesets()[0].clone();
    let tilesheet_image = tilesheet.image.as_ref().unwrap();
    let texture_name = tilesheet_image.source.to_str().unwrap(); 
    let texture_width = tilesheet_image.width; 
    let tile_width = tilesheet.tile_width;
    let tile_height = tilesheet.tile_height;
    let spacing = tilesheet.spacing;
    let margin = tilesheet.margin;
    let tiles_per_row = (texture_width as u32 - margin + spacing) / (tile_width + spacing);


    // suposes that exists 1 layer
    for layer in map.layers() {
        if let tiled::LayerType::TileLayer(tiled::TileLayer::Finite(data)) = layer.layer_type()
        {
            let (width, height) = (data.width() as usize, data.height() as usize);
            for x in 0..width as i32 {
                for y in 0..height as i32 {
                    if let Some(tile) = data.get_tile(x, y) {
                        world.create_entity()
                            .with(Position {
                                x: (x * tile_width as i32) as f64,
                                y: (y * tile_height as i32) as f64,
                            })
                            .with(Renderable {
                               texture_name: texture_name.to_string(),
                               src_x: (tile.id() % tiles_per_row * tile_width) as i32,
                               src_y: (tile.id() / tiles_per_row * tile_height) as i32,
                               src_width: tile_width,
                               src_height: tile_height,
                               dest_width: tile_width,
                               dest_height: tile_height,
                               frame: 0,
                               total_frames: 1,
                               rot: 0.0,
                            }).build();
                    }
                }
            }
        }
    }

    Ok(())
}
