pub mod game;
pub mod utils;

fn main() -> Result<(), String> {
    game::run()
}
